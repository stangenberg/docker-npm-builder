FROM thstangenberg/baseimage

MAINTAINER Thorben Stangenberg <thorben@stangenberg.ch>

ENV NVM_DIR /opt/nvm

RUN apt-get update

RUN apt-get install -y git && \
	apt-get install -y build-essential && \
	apt-get install -y libssl-dev

RUN git clone https://github.com/creationix/nvm.git $NVM_DIR

RUN echo ". $NVM_DIR/nvm.sh" >> /root/.bashrc

RUN echo "nvm use 0.12" >> /root/.bashrc

# install nodejs
RUN . $NVM_DIR/nvm.sh && nvm install 0.12

# install bower
RUN . $NVM_DIR/nvm.sh && nvm use 0.12 && cd /tmp && npm install bower@">=1.3.0 <1.4.0" -g

# install gulp
RUN . $NVM_DIR/nvm.sh && nvm use 0.12 && cd /tmp && npm install gulp@">=3.8.0 <3.9.0" -g 

# install phantomjs
RUN . $NVM_DIR/nvm.sh && nvm use 0.12 && cd /tmp && npm install phantomjs@">=1.9.0 <2.0.0" -g

# # install testem
RUN . $NVM_DIR/nvm.sh && nvm use 0.12 && cd /tmp && npm install testem@">=0.7.0 <0.8.0" -g

# housekeeping
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
