# Node.js builder

CI image for [Node][] on Ubuntu 14.04.


## Installed Node Versions

The node versions are managed by [][nvm].
Use `nvm use VERSION` to configure nodejs.


### Available versions

- 0.12


## Installed packages

- [][bower] 1.3.x
- [][phantomjs] 2.0.x
- [][gulp] 3.8.x
- [][testem] 1.3.x


## License

[][LICENSE]


[Node]: https://nodejs.org/ "Node.js"
[bower]: http://bower.io/ "Bower"
[phantomjs]: http://phantomjs.org/ "PhantomJS"
[gulp]: http://gulpjs.com/ "Gulp"
[testem]: https://github.com/airportyh/testem "Test'em"
[LICENSE]: https://bitbucket.org/thstangenberg/docker-npm-builder/src/master/LICENSE.md "Published under the MIT License"
[nvm]: https://github.com/creationix/nvm "Node Version Manager"